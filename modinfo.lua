-- This information tells other players more about the mod
name = "    Miner protection"
description = "Miner helmet prevents falling rock damage."
author = "Kelton"
version = "1.0"

forumthread = ""

dont_starve_compatible = true
reign_of_giants_compatible = true
shipwrecked_compatible = true

api_version = 6

-- Can specify a custom icon for this mod!
icon_atlas = "modicon.xml"
icon = "modicon.tex"