local ROG_ENABLED = GLOBAL.IsDLCEnabled(GLOBAL.REIGN_OF_GIANTS)
local SW_ENABLED = GLOBAL.IsDLCEnabled(GLOBAL.CAPY_DLC)
local HAM_ENABLED = GLOBAL.IsDLCEnabled(GLOBAL.PORKLAND_DLC)
local ANYDLC = ROG_ENABLED or SW_ENABLED or HAM_ENABLED

function quakerpostinit(inst)


local function grounddetection_update(inst)
	local pt = Point(inst.Transform:GetWorldPosition())
	
	if not inst.shadow then
		GiveDebrisShadow(inst)
	else
		UpdateShadowSize(inst, pt.y)
	end

	if pt.y < 2 then
	if 
			inst.fell = true
		end
		inst.Physics:SetMotorVel(0,0,0)
    end

	if pt.y <= .2 then
		PlayFallingSound(inst)
		if inst.shadow then
			inst.shadow:Remove()
		end

		local ents = TheSim:FindEntities(pt.x, 0, pt.z, 2, nil, {'smashable'})
	    for k,v in pairs(ents) do
	    	if v and v.components.combat and v ~= inst then  -- quakes shouldn't break the set dressing
				if not v.components.inventory:GetEquippedItem(GLOBAL.EQUIPSLOTS.HEAD) == "minerhat" then
					v.components.combat:GetAttacked(inst, 20, nil)
				end
	    	end
	   	end
	   	--play hit ground sound


	   	inst.Physics:SetDamping(0.9)	   	

	    if inst.updatetask then
			inst.updatetask:Cancel()
			inst.updatetask = nil
		end

		if math.random() < 0.75 and not (inst.prefab == "mole" or inst.prefab == "rabbit") then
			--spawn break effect
			inst.SoundEmitter:PlaySound("dontstarve/common/stone_drop")
			local pt = Vector3(inst.Transform:GetWorldPosition())
			local breaking = SpawnPrefab("ground_chunks_breaking")
			breaking.Transform:SetPosition(pt.x, 0, pt.z)
			inst:Remove()
		end
	end

	-- Failsafe: if the entity has been alive for at least 1 second, hasn't changed height significantly since last tick, and isn't near the ground, remove it and its shadow
	if inst.last_y and pt.y > 2 and inst.last_y > 2 and (inst.last_y - pt.y  < 1) and inst:GetTimeAlive() > 1 and not inst.fell then
		if inst.shadow then
			inst.shadow:Remove()
		end
		inst:Remove()
	end
	inst.last_y = pt.y
end


end

AddComponentPostInit("quaker", quakerpostinit)